tablix2 (0.3.5-7) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Fix day-of-week for changelog entry 0.3.1-1.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 14:09:40 +0100

tablix2 (0.3.5-6) unstable; urgency=medium

  * QA upload.
  * Add Breaks+Replaces for the separation of the -doc package.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 29 Aug 2022 22:45:17 +0200

tablix2 (0.3.5-5) unstable; urgency=medium

  * QA upload.
  * Source only upload to enable testing migration.
  * Adding a packaging VCS:
    - d/control: Add VCS-* fields
    - enabling CI on salsa
    - add gbp.conf

 -- Tobias Frost <tobi@debian.org>  Wed, 24 Aug 2022 07:46:30 +0200

tablix2 (0.3.5-4) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group (see #994647).
  * Convert package to '3.0 (quilt)' format (Closes: #1007512).
  * debian/control:
    + Priority: extra -> optional.
    + Switch to debhelper-compat v13 and drop debian/compat file.
    + Build-Depends: add pkg-config; remove dh-autoreconf.
    + Raise Standards-Version to 4.6.1 from 3.9.8 (no changes needed).
    + Update package description.
    + Use HTTPS protocol.
    + Add documentation and examples package (tablix2-doc).
    + Declare Rules-Requires-Root: no.
    + Trim trailing whitespace.
    + Suggest gnuplot instead of Recommending it.
  * debian/copyright: Convert to DEP-5 format and update.
  * debian/patches:
    + Remove references to 'debian' from upstream build files.
    + Remove hard-coded 'localedir' override from configure.in.
    + Use pkg-config to find libxml2 (Closes: #949502).
    + Fix spelling and syntax errors in the man pages.
  * debian/rules:
    + Use the 'dh' build system (Closes: #949600).
    + Add hardening flags to DEB_BUILD_MAINT_OPTIONS.
  * debian/upstream: Add metadata file.
  * debian/watch: Update version, repository URL and archive regex.
  * tablix2: Don't install the *.la files (Closes: #810271).
  * tablix2.docs: Add doc-base file.
  * Override some Lintian warnings.

 -- Hugh McMaster <hugh.mcmaster@outlook.com>  Sat, 20 Aug 2022 20:06:29 +1000

tablix2 (0.3.5-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS on ppc64el and arm64. (Closes: #756442)

 -- Breno Leitao <leitao@debian.org>  Mon, 05 Sep 2016 15:48:40 -0400

tablix2 (0.3.5-3) unstable; urgency=medium

  * maintenance and cleanup spin:
  * remove boilerplate from watch file
  * moved homepage to correct place in debian/control
  * added debian/source/format
  * add default targets to debian/rules
  * switch on hardening
  * bumped standards-version

 -- Robert Lemmen <robertle@semistable.com>  Thu, 02 Jun 2016 21:00:15 +0100

tablix2 (0.3.5-2) unstable; urgency=low

  * Set priority to extra
  * Bumped Standards-Version to 3.8.3 and related changes

 -- Robert Lemmen <robertle@semistable.com>  Mon, 11 Jan 2010 15:32:41 +0000

tablix2 (0.3.5-1) unstable; urgency=low

  * New upstream release (closes: #439886)
  * fixed build system to make it re-buildable without cleans

 -- Robert Lemmen <robertle@semistable.com>  Tue, 18 Sep 2007 15:54:15 +0200

tablix2 (0.3.3-1) unstable; urgency=low

  * New upstream release (closes: #388777)
  * Improvements to the Debian packaging (control- and rules-file)
  * Sponsored upload by Petter Reinholdtsen

 -- Robert Lemmen <robertle@semistable.com>  Thu,  5 Oct 2006 22:42:10 +0100

tablix2 (0.3.1-1) unstable; urgency=low

  * New upstream release
  * Upload sponsored by Petter Reinholdtsen

 -- Robert Lemmen <robertle@semistable.com>  Thu, 06 Apr 2006 11:40:50 +0100

tablix2 (0.3.0-2) unstable; urgency=low

  * Changed priority to extra, would violate policy otherwise because it
    depends on an extra package.
  * Sponsored upload by Petter Reinholdtsen

 -- Robert Lemmen <robertle@semistable.com>  Tue, 29 Nov 2005 10:30:39 +0100

tablix2 (0.3.0-1) unstable; urgency=low

  * Repacked for debian (closes: #322151)
  * Sponsored upload by Petter Reinholdtsen

 -- Robert Lemmen <robertle@semistable.com>  Sun, 13 Nov 2005 16:50:22 +0200

tablix (0.1.2-2) unstable; urgency=low

  * Minor cleanups in the debian/ dir
  * Included the tablix_test manpage from ubuntu

 -- Robert Lemmen <robertle@semistable.com>  Tue,  9 Aug 2005 13:23:01 +0200

tablix (0.1.2-1) unstable; urgency=low

  * New upstream release

 -- Tomaz Solc <avian@orion.tomysoftware.net>  Fri, 10 Jun 2005 12:57:36 +0200

tablix (0.1.1-1) unstable; urgency=low

  * New upstream release.

 -- Tomaz Solc <tomaz.solc@siol.net>  Sat, 29 Jan 2005 17:13:08 +0100

tablix (0.1.0-1) unstable; urgency=low

  * New upstream release.

 -- Tomaz Solc <avian@orion.tomysoftware.net>  Sat, 23 Oct 2004 17:24:47 +0200

tablix (0.0.8-1) unstable; urgency=low

  * New upstream release.

 -- Tomaz Solc <avian@orion.tomysoftware.net>  Sat, 31 Jul 2004 11:08:17 +0200

tablix (0.0.7-1) unstable; urgency=low

  * New upstream release.

 -- Tomaz Solc <tomaz.solc@siol.net>  Wed, 31 Mar 2004 19:12:17 +0200

tablix (0.0.6-1) unstable; urgency=low

  * New upstream release.

 -- Tomaz Solc <tomaz.solc@siol.net>  Sat, 28 Feb 2004 12:17:35 +0100

tablix (0.0.5-1) unstable; urgency=low

  * New upstream release.

 -- Tomaz Solc <tomaz.solc@siol.net>  Sun, 26 Oct 2003 11:03:24 +0100

tablix (0.0.4-1) unstable; urgency=low

  * New upstream release.

 -- Tomaz Solc <tomaz.solc@siol.net>  Mon,  8 Sep 2003 21:46:22 +0200

tablix (0.0.2-1) unstable; urgency=low

  * Initial Release.

 -- Tomaz Solc <tomaz.solc@siol.net>  Fri,  6 Sep 2002 17:08:56 +0200
